## Software Development Life Cycle ##
SDLC is a blueprint for creating a software application. 
SDLC is a cost effective and time-efficient process tha development teams use to design and build hig-quality software. The goal of SDLC is to minimize project risks through forward planning so that software meets teh custome expectations duing production and beyond.

### Importance of SDLC ###

SDLC provides a systematic framework with specific deliverables at each stage of the s/w development process. In this process all the stakeholders agree on s/w development goals and requirements upfront and also have a plan to achieve those goals.

SDLC helps in 
- Increased visibility of the development process for all stakeholders involved. Helps in clarity of Goal setting
- Efficient estimation, planning and scheduling.
- Clear stage progression
- Improved risk management, cost estimation and proper testing
- Systematic s/w delivery and better customer satisfaction 
- Perfection is achievable

### Phases of SDLC ###

SDLC outlines several tasks required to build a s/w application. The exact number and nature of steps depend on the business and its product goals. But on average most companies define SDLC with 5 to 7 phases. More complex projects reach 10 or more stages.

7 Stages of SDLC are
1. Planning
    Define the problem and scope of the existing systems and determine the objectives for building new systems.
    Tasks included - cost-benefit Analysis, scheduling estimation and allocation. 

2. Requirement Analysis
    Here the s/w developer gathers all the specific details required for the new system. The first prototype ideas are also discussed in this stage.
        - Define system requirements for prototypes
        - Find alternatives for existing prototypes
        - Determine the user requrements

    Team also collects requirements from several stakeholders such as customers, internal and external experts and mangagers to create a s/w requiement specification(SRS) document. This document sets the expectations and defines common goals that aid in project planning which includes estimating costs, creating schedule and detailed plan to achieve goal.

3. Design and prototype
    The primary purpose is to prepare a detailed design specifications document that covers all requirements
    Here the s/w engineer analyze requirements and identify the best solutions to create the software. 
    One or more designs are created to achieve the desired results. Design can include diagrams, flow charts, sketches and schemas. Also fisrt outline of overall application and design of 
        - Databases, UI, System Interface, Network and network requirements

4. Software development stage
    Coders actually start writing code for the custom software. The s/w engineer refers to the documents created till desgin phase for better understanding of what to build and how to build. The coders build the application based on the coding standards defined by the organization.

5. Software Testing
    The development team combines automation and manual testing to check the software for bugs. 
    QA team checks for errors and checks if the software meets the clients requirements. Testing often runs parallel to development/coding stage as teams usually test their code immediately after writing. 

6 Implementation and integration
    Build/testing environment: The software on which the developer team is working on to develop and test the code.

    Production: The software that the customers use.

    Separate environments ensures that customer continues to use the software while it is being changed or upgraded. 

7. Operations & Maintenance
    In this stage the teams work on fixing bugs, resolving customer issues and implementing changes. The team also monitors the overall system performance, security and user experience to improve the existing software.

- Software development team follows different environments in SDLC like **Dev, SIT, PROD, UAT**

- Top Software Development Life Cycle Tools are-

**Jira, Git, Asana, Confluence**

**HLD (High Level Design) provides system design for database and functional architecture for all modules and sub-mobules.**

**LLD(Low level Design) provides the detailed design for modules and programs, including logic for each component and verifying module specifications.**


## SDLC Methodology ##

**Agile methodology**
This model breaks SDLC phases into several development cycles. The team iterates through phases rapidly by delivering small, incremental software changes in each cycle. 
Requirements, plans and results are continously evaluated which helps in quick response to the change. Agile model is both iterative and incremental. This is the most efficient model.

Based on the Agile methodologies, many teams are also apply Agile Framework known as Scrum to help stucture more complex development projects. Scrum teams work in sprints, which usually last 2 to 4 weeks to complete the assigned tasks. 