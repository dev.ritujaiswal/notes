How to delegate like a boss (without losing control)

1. Avoid micromanagement and increase self-accountability with advanced checklists
    Create a culture of accountability by creating a checklists with Trello Premium. Adding members and due dates to subtasks will take self-accountability to another level. 
2. Keep critical project information front and center with Custom Fields

3. Automate routine tasks and free up time for creativity
    Automation is like your robo-sidekick for team productivity, helping you create custom automations and rules to streamline your workflow and free up time for other activities. 
4. Get all your moving parts in line with Timeline view
    Planning a project that involves multiple tasks and deadlines, not to mention different teams, requires an elevated perspective.

    Timeline view makes it easy to see how all the different elements of your project work together:

    Add a start date or due date to view your card within the project timeline.
    Click on any card in Timeline view to add more details.
    Drag and drop cards to make adjustments on the fly. 
    View projects in monthly, quarterly, or yearly overviews.
    Focus on specific aspects of the project by sorting by list, member, or label.
5. Don't let a due date slip by–let Calendar view keep you on track 
    Calendar view allows you to see cards in a date-forward format to more easily visualize when things need to get done. So it allows you to track scheduled work and manage deadlines, but it can also be used for individual task management. 
6. Speed up work with Table view
    With Table view, managing tasks and cards in a list format lets you get a quick overview and also generate useful insights for your team. Sort by due dates to evaluate the progress of your project and enjoy a single view at the work in the pipeline.
7. Make space for location-based jobs with Map view
    For your projects where location is key, all of your important data with geographical context, such as  field sales, service teams on the road, and events, can be tracked with Map view. 

    Cards that have location-based information will automatically appear on the Map view.  From there, you can see the cards and do some quick edits if you wish, such as adding a label. Add new locations or cards with location-based information straight from the Map view as well.

8. Optimize your team’s performance with Dashboard view
    When you need to manage workloads and visualize key metrics, such as due dates or assigned cards, Dashboard view can provide workload insights, insight into potential bottlenecks, and other key project info. You’ll be presented with a bar chart, pie charts, and more that you can customize and use at any time during the project. 

9. Increase oversight without losing track of details with Workspace views

10. Stay on top of changes with Dashcards


